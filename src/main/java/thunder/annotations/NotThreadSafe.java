package thunder.annotations;

import java.lang.annotation.*;

/**
 * Radek Starczynowski
 * <radek.starczynowski@gmail.com>
 * 01.10.14, 17:21
 */
@Documented
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.CONSTRUCTOR,
        ElementType.METHOD, ElementType.TYPE, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.SOURCE)
public @interface NotThreadSafe {
}
